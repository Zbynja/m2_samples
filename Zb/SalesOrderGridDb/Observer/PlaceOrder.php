<?php

namespace Zb\SalesOrderGridDb\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\ResourceModel\GridInterface as OrderGridUpdater;
use Psr\Log\LoggerInterface;

/**
 * Class PlaceOrder
 * Sync new columns data with order grid db table when a new order is created and placed.
 * @package Zb\SalesOrderGridDb\Observer
 */
class PlaceOrder implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ScopeConfigInterface
     */
    private $globalConfig;

    /**
     * @var OrderGridUpdater
     */
    private $orderGridUpdater;

    /**
     * PlaceOrder constructor.
     * @param OrderGridUpdater $orderGridUpdater
     * @param ScopeConfigInterface $globalConfig
     * @param LoggerInterface $logger
     */
    public function __construct(
        OrderGridUpdater $orderGridUpdater,
        ScopeConfigInterface $globalConfig,
        LoggerInterface $logger
    ) {
        $this->orderGridUpdater = $orderGridUpdater;
        $this->globalConfig = $globalConfig;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        // if update on schedule is ON, exit
        if ($this->globalConfig->getValue('dev/grid/async_indexing')) {
            return;
        }

        // if no orders found, exit
        $orders = $this->extractOrders(
            $observer->getEvent()
        );
        if (! $orders) {
            return;
        }

        foreach ($orders as $order) {
            $this->syncOrderData($order);
        }
    }

    /**
     * Sync the order data with the orders grid table
     *
     * @param OrderInterface $order
     * @return void
     * @throws NotFoundException
     */
    private function syncOrderData($order)
    {
        $orderId = $order->getEntityId();
        if (! $orderId) {
            return;
        }

        try {
            $this->orderGridUpdater->refresh($orderId);
        } catch (AlreadyExistsException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Returns Orders entity list from Event data container
     *
     * @param Event $event
     * @return OrderInterface[]|null
     */
    private function extractOrders(Event $event)
    {
        $order = $event->getData('order');
        if (null !== $order) {
            return [$order];
        }

        return $event->getData('orders');
    }
}
