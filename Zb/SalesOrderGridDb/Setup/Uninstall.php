<?php
namespace Zb\SalesOrderGridDb\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Uninstall
 * @package Zb\SalesOrderGridDb\Setup
 */
class Uninstall implements UninstallInterface
{
    /**
     * @inheritdoc
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        /** @var AdapterInterface $connection */
        $connection = $installer->getConnection('sales');

        $ordersGridTable = $setup->getTable('sales_order_grid');

        if ($connection->tableColumnExists($ordersGridTable, 'coupon_code')) {
            $connection->dropColumn($ordersGridTable, 'coupon_code');
        }
        if ($connection->tableColumnExists($ordersGridTable, 'discount_amount')) {
            $connection->dropColumn($ordersGridTable, 'discount_amount');
        }

        $installer->endSetup();
    }
}
