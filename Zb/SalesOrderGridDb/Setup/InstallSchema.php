<?php
namespace Zb\SalesOrderGridDb\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Add extra data into sales_order_grid table
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @inheritdoc
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var AdapterInterface $connection */
        $connection = $setup->startSetup()->getConnection('sales');

        $connection->addColumn(
            $setup->getTable('sales_order_grid'),
            'coupon_code',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Coupon Code'
            ]
        );
        $connection->addColumn(
            $setup->getTable('sales_order_grid'),
            'discount_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'comment' => 'Discount Amount'
            ]
        );
    }
}
